package radixsort_ejercicio2;
import java.util.*;
/**
 *
 * @author hitzu
 */
public class RADIXSORT_EJERCICIO2 {

     static class DateItem {
        String datetime;

        DateItem(String date) {
            this.datetime = date;
        }
    }

    static class SortByDate implements Comparator<DateItem> {
        @Override
        public int compare(DateItem a, DateItem b) {
            return b.datetime.compareTo(a.datetime);
        }
    }
    public static void main(String[] args) {
        
        
        
        List<DateItem> dateList = new ArrayList<>();
        dateList.add(new DateItem("2006-03-31"+" Contrato 1"));
        dateList.add(new DateItem("2009-01-18"+" Contrato 2"));
        dateList.add(new DateItem("2020-12-17"+" Contrato 3"));
        dateList.add(new DateItem("2021-07-25"+" Contrato 4"));
        dateList.add(new DateItem("2014-02-26"+" Contrato 5"));
        dateList.add(new DateItem("2009-05-06"+" Contrato 6"));
        dateList.add(new DateItem("2019-08-05"+" Contrato 7"));
        dateList.add(new DateItem("2018-01-26"+" Contrato 8"));
        dateList.add(new DateItem("2012-01-16"+" Contrato 9"));
        dateList.add(new DateItem("2019-07-26"+" Contrato 10"));
        dateList.add(new DateItem("2003-09-19"+" Contrato 11"));
        dateList.add(new DateItem("2013-04-07"+" Contrato 12"));
        dateList.add(new DateItem("2007-05-26"+" Contrato 13"));
        dateList.add(new DateItem("2012-10-25"+" Contrato 14"));
        dateList.add(new DateItem("2019-03-15"+" Contrato 15"));
        dateList.add(new DateItem("2000-12-04"+" Contrato 16"));
        dateList.add(new DateItem("2008-11-26"+" Contrato 17"));
        dateList.add(new DateItem("2003-12-12"+" Contrato 18"));
        dateList.add(new DateItem("2003-01-26"+" Contrato 19"));
        dateList.add(new DateItem("2015-04-26"+" Contrato 20"));
        dateList.add(new DateItem("2014-06-25"+" Contrato 21"));
        dateList.add(new DateItem("2014-07-03"+" Contrato 22"));
        dateList.add(new DateItem("2002-12-08"+" Contrato 23"));
        dateList.add(new DateItem("2012-08-26"+" Contrato 24"));
        dateList.add(new DateItem("2004-11-26"+" Contrato 25"));
        dateList.add(new DateItem("2011-11-09"+" Contrato 26"));
        dateList.add(new DateItem("2011-04-11"+" Contrato 27"));
        dateList.add(new DateItem("2006-07-31"+" Contrato 28"));
        dateList.add(new DateItem("2008-09-30"+" Contrato 29"));
        dateList.add(new DateItem("2004-05-01"+" Contrato 30"));
        dateList.add(new DateItem("2010-04-25"+" Contrato 31"));
        dateList.add(new DateItem("2012-11-27"+" Contrato 32"));
        dateList.add(new DateItem("2014-10-10"+" Contrato 33"));
        dateList.add(new DateItem("2004-10-14"+" Contrato 34"));
        dateList.add(new DateItem("2000-01-21"+" Contrato 35"));
        dateList.add(new DateItem("2004-04-20"+" Contrato 36"));
        dateList.add(new DateItem("2013-01-27"+" Contrato 37"));
        dateList.add(new DateItem("2020-07-13"+" Contrato 38"));
        dateList.add(new DateItem("2013-02-28"+" Contrato 39"));
        dateList.add(new DateItem("2010-11-26"+" Contrato 40"));
        dateList.add(new DateItem("2009-04-15"+" Contrato 41"));
        dateList.add(new DateItem("2003-06-14"+" Contrato 42"));
        dateList.add(new DateItem("2015-03-07"+" Contrato 43"));
        dateList.add(new DateItem("2014-02-09"+" Contrato 44"));
        dateList.add(new DateItem("2019-08-16"+" Contrato 45"));
        dateList.add(new DateItem("2004-12-13"+" Contrato 46"));
        dateList.add(new DateItem("2014-09-17"+" Contrato 47"));
        dateList.add(new DateItem("2000-06-23"+" Contrato 48"));
        dateList.add(new DateItem("2000-07-02"+" Contrato 49"));
        dateList.add(new DateItem("2014-04-12"+" Contrato 50"));
        Collections.sort(dateList, new SortByDate());
        
        dateList.forEach(date -> {
            
            
            System.out.println(date.datetime);
        });
        System.out.println();
    }
    
}
